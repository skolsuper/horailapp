package com.projects.james.ticketdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import edu.cmu.pocketsphinx.demo.R;

import static android.widget.Toast.makeText;

public class ConfirmActivity extends AppCompatActivity {

    private String mOrigin;
    private String mDestination;
    private String mTicketType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);

        final Bundle extras = getIntent().getExtras();
        mOrigin = extras.getString(getString(R.string.origin_key));
        mDestination = extras.getString(getString(R.string.destination_key));
        mTicketType = extras.getString(getString(R.string.ticket_type_key));

        ((TextView) findViewById(R.id.confirmOrigin)).setText(mOrigin);
        ((TextView) findViewById(R.id.confirmDestination)).setText(mDestination);
        ((TextView) findViewById(R.id.confirmTicketType)).setText(mTicketType);

        Button startOver = (Button) findViewById(R.id.startOver);
        startOver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfirmActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        Button proceed = (Button) findViewById(R.id.proceedPayment);
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = "Demo only. Real app will start payment process for the following ticket:\n"
                        + mTicketType + " from " + mOrigin + " to " + mDestination;
                makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
