/* ====================================================================
 * Copyright (c) 2014 Alpha Cephei Inc.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALPHA CEPHEI INC. ``AS IS'' AND
 * ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CARNEGIE MELLON UNIVERSITY
 * NOR ITS EMPLOYEES BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ====================================================================
 */

package com.projects.james.ticketdemo;

import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.collections.iterators.ArrayIterator;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import edu.cmu.pocketsphinx.demo.R;

import static android.widget.Toast.makeText;
import static edu.cmu.pocketsphinx.SpeechRecognizerSetup.defaultSetup;

public class ListenActivity extends AppCompatActivity implements
        RecognitionListener {

    private static final String JOURNEY_SEARCH = "journey";
    private static final String CONFIRMATION_SEARCH = "confirm";

    private String INVALID_JOURNEY;
    private List<String> STATIONS;
    private List<String> TICKETS;
    private List<String> CONFIRM_WORDS;
    private int LISTEN_TIMEOUT;

    private SpeechRecognizer recognizer;
    private TextToSpeech ttsobj;

    private String mTicketType;
    private String mDestination;
    private String mOrigin;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);

        setContentView(R.layout.activity_listen);

        final Resources res = getResources();
        LISTEN_TIMEOUT = res.getInteger(R.integer.listen_timeout);
        INVALID_JOURNEY = res.getString(R.string.invalid_journey);
        STATIONS = Arrays.asList(res.getStringArray(R.array.stations));
        TICKETS = Arrays.asList(res.getStringArray(R.array.ticket_types));
        CONFIRM_WORDS = Arrays.asList(res.getStringArray(R.array.confirm_words));

        ((TextView) findViewById(R.id.caption_text))
                .setText(R.string.recognizer_prepare);

        // Recognizer initialization is a time-consuming and it involves IO,
        // so we execute it in async task

        new AsyncTask<Void, Void, Exception>() {
            @Override
            protected Exception doInBackground(Void... params) {
                try {
                    Assets assets = new Assets(ListenActivity.this);
                    File assetDir = assets.syncAssets();
                    setupRecognizer(assetDir);
                } catch (IOException e) {
                    return e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Exception result) {
                if (result != null) {
                    ((TextView) findViewById(R.id.caption_text))
                            .setText(String.format(getString(R.string.failed_init_recognizer), result));
                } else {
                    recognizer.startListening(JOURNEY_SEARCH, LISTEN_TIMEOUT);
                    ((TextView) findViewById(R.id.caption_text)).setText(R.string.start_speaking);
                }
            }
        }.execute();

        ttsobj = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    ttsobj.setLanguage(Locale.UK);
                    ttsobj.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                        @Override
                        public void onStart(String utteranceId) {}

                        @Override
                        public void onDone(String utteranceId) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    recognizer.startListening(CONFIRMATION_SEARCH, LISTEN_TIMEOUT);
                                    ((TextView) findViewById(R.id.caption_text)).setText(R.string.confirm_instructions);
                                }
                            });
                        }

                        @Override
                        public void onError(String utteranceId) {}
                    });
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        recognizer.cancel();
        recognizer.shutdown();
        ttsobj.stop();
        ttsobj.shutdown();
    }
    
    /**
     * In partial result we get quick updates about current hypothesis. In
     * keyword spotting mode we can react here, in other modes we need to wait
     * for final result in onResult.
     */
    @Override
    public void onPartialResult(Hypothesis hypothesis) {
        if (hypothesis == null || recognizer.getSearchName().equals(CONFIRMATION_SEARCH))
    	    return;

        String text = hypothesis.getHypstr();
        ((TextView) findViewById(R.id.result_text)).setText(text);
    }

    /**
     * This callback is called when we stop the recognizer.
     */
    @Override
    public void onResult(Hypothesis hypothesis) {

        switch (recognizer.getSearchName()) {
            case JOURNEY_SEARCH:
                ((TextView) findViewById(R.id.result_text)).setText("");
                if (hypothesis == null) {
                    recognizer.startListening(JOURNEY_SEARCH, LISTEN_TIMEOUT);
                    return;
                }

                String text = hypothesis.getHypstr();
                ArrayIterator words = new ArrayIterator(text.split(" "));
                try {
                    mOrigin = getNextMatch(words, STATIONS);
                    mDestination = getNextMatch(words, STATIONS);
                    if (mDestination.equals(mOrigin)) throw new Exception(INVALID_JOURNEY);
                    mTicketType = getNextMatch(words, TICKETS);
                } catch (Exception a) {
                    makeText(getApplicationContext(), a.getMessage(), Toast.LENGTH_SHORT).show();
                    recognizer.startListening(JOURNEY_SEARCH, LISTEN_TIMEOUT);
                    return;
                }
                text = mTicketType + " from " + mOrigin + " to " + mDestination;
                makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();

                HashMap<String, String> params = new HashMap<>();
                params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "confirmationUtterance");

                ttsobj.speak(text, TextToSpeech.QUEUE_FLUSH, params);
                break;
            case CONFIRMATION_SEARCH:
                if (hypothesis == null) {
                    recognizer.startListening(CONFIRMATION_SEARCH, LISTEN_TIMEOUT);
                    return;
                }
                final String confirmText = hypothesis.getHypstr();
                Log.d("Confirmation Search", "onResult: " + confirmText);
                if (CONFIRM_WORDS.contains(confirmText)) {
                    Intent intent = new Intent(ListenActivity.this, ConfirmActivity.class);
                    intent.putExtra(getString(R.string.origin_key), mOrigin);
                    intent.putExtra(getString(R.string.destination_key), mDestination);
                    intent.putExtra(getString(R.string.ticket_type_key), mTicketType);
                    startActivity(intent);
                    finish();
                } else {
                    recognizer.startListening(JOURNEY_SEARCH, LISTEN_TIMEOUT);
                    ((TextView) findViewById(R.id.caption_text)).setText(R.string.start_speaking);
                    return;
                }
                break;
        }
    }

    private String getNextMatch(ArrayIterator it, List<String> possible) throws Exception {
        String word = (String) it.next();
        while (!possible.contains(word)) {
            try {
                word = word.concat(" " + it.next());
            } catch (NoSuchElementException e) {
                throw new Exception(INVALID_JOURNEY);
            }
        }
        return word;
    }

    @Override
    public void onBeginningOfSpeech() {}

    /**
     * We stop recognizer here to get a final result
     */
    @Override
    public void onEndOfSpeech() {
        recognizer.stop();
    }

    private void setupRecognizer(File assetsDir) throws IOException {
        // The recognizer can be configured to perform multiple searches
        // of different kind and switch between them
        
        recognizer = defaultSetup()
                .setAcousticModel(new File(assetsDir, "en-us-ptm"))
                .setDictionary(new File(assetsDir, "raildict.dict"))
                
                // To disable logging of raw audio comment out this call (takes a lot of space on the device)
                //.setRawLogDir(assetsDir)
                
                // Use context-independent phonetic search, context-dependent is too slow for mobile
                .setBoolean("-allphone_ci", true)
                
                .getRecognizer();
        recognizer.addListener(this);

        // Create grammar-based search for digit recognition
        File journeyGrammar = new File(assetsDir, "journey.gram");
        recognizer.addGrammarSearch(JOURNEY_SEARCH, journeyGrammar);

        File confirmationGrammar = new File(assetsDir, "confirmation.gram");
        recognizer.addGrammarSearch(CONFIRMATION_SEARCH, confirmationGrammar);
    }

    @Override
    public void onError(Exception error) {
        ((TextView) findViewById(R.id.caption_text)).setText(error.getMessage());
    }

    @Override
    public void onTimeout() {
        Log.d("SpeechRecognizerTimeout", "Timeout");
        Intent intent = new Intent(ListenActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
