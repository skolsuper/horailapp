package com.projects.james.ticketdemo;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.Arrays;

import edu.cmu.pocketsphinx.demo.R;
import fr.ganfra.materialspinner.MaterialSpinner;

public class MainActivity extends AppCompatActivity {

    private String mOrigin;
    private String mDestination;
    private String mTicketType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        MaterialSpinner originSpinner = (MaterialSpinner) findViewById(R.id.origin);
        MaterialSpinner destinationSpinner = (MaterialSpinner) findViewById(R.id.destination);
        MaterialSpinner ticketTypeSpinner = (MaterialSpinner) findViewById(R.id.ticketType);

        ArrayAdapter<CharSequence> stationAdapter = ArrayAdapter.createFromResource(
                this,
                R.array.stations,
                android.R.layout.simple_spinner_item);
        stationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        originSpinner.setAdapter(stationAdapter);
        destinationSpinner.setAdapter(stationAdapter);

        ArrayAdapter<CharSequence> ticketTypeAdapter = ArrayAdapter.createFromResource(
                this,
                R.array.ticket_types,
                android.R.layout.simple_spinner_item);
        ticketTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ticketTypeSpinner.setAdapter(ticketTypeAdapter);

        originSpinner.setOnItemSelectedListener(spinnerListener);
        destinationSpinner.setOnItemSelectedListener(spinnerListener);
        ticketTypeSpinner.setOnItemSelectedListener(spinnerListener);

        ImageButton startDemoBtn = (ImageButton) findViewById(R.id.startDemoBtn);
        startDemoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListenActivity.class);
                startActivity(intent);
            }
        });
    }

    private AdapterView.OnItemSelectedListener spinnerListener;

    {
        spinnerListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String[] stationArray = getResources().getStringArray(R.array.stations);
                ArrayList<String> stations = new ArrayList<>(Arrays.asList(stationArray));
                ArrayAdapter<String> adapter = new ArrayAdapter<>(
                        MainActivity.this,
                        android.R.layout.simple_spinner_item,
                        stations);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                String selected = (String) parent.getItemAtPosition(position);
                switch (parent.getId()) {
                    case R.id.origin:
                        mOrigin = selected;
                        if (!selected.equals(stationArray[0])) {
                            adapter.remove(selected);
                            MaterialSpinner destinationSpinner = (MaterialSpinner) findViewById(R.id.destination);
                            destinationSpinner.setAdapter(adapter);
                            destinationSpinner.setSelection(adapter.getPosition(mDestination));
                        }
                        break;
                    case R.id.destination:
                        mDestination = selected;
                        break;
                    case R.id.ticketType:
                        mTicketType = selected;
                        break;
                }
                findViewById(R.id.confirmSelectionBtn).setEnabled(checkDisplayNextBtn());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        };
    }

    private boolean checkDisplayNextBtn() {
        Resources res = getResources();
        final String unselected = res.getStringArray(R.array.stations)[0];
        return !(mOrigin == null || mOrigin.equals(unselected) ||
                mDestination == null || mDestination.equals(unselected) ||
                mTicketType == null || mTicketType.equals(res.getStringArray(R.array.ticket_types)[0]));
    }
}
